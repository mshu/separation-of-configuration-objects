import xml.dom.minidom
 
def main():
	filename = "MergeSettingsConfigurationБухгалтерияПредприятияКОРП3.0.76.6_result.xml"

    # используем функцию parse() для загрузки и парсинга XML файла
    f = open(filename, "r", encoding='utf-8')
    xmlStr = f.read()
    doc = xml.dom.minidom.parseString(xmlStr)
    all_modules_path = "result_modules.txt"
    
    # получим список объектов для загрузки
    all_modules_file = open(all_modules_path, 'r', encoding='utf-8')
    all_modules_line = all_modules_file.readline()

    list_objects = list()

    while all_modules_line:
        list_objects.append('CommonModule.' + all_modules_line.strip())
        all_modules_line = all_modules_file.readline()

    all_modules_file.close()

      # получаем список тегов XML из документа
    object_tag = doc.getElementsByTagName("Object")
    for object_item in object_tag:
        if object_item.getAttribute("fullNameInSecondConfiguration") in list_objects:
            parent = object_item.parentNode
            parent.removeChild(object_item)
   

    file_handle = open(filename, "w")
    doc.writexml(file_handle, encoding="utf-8")
    file_handle.close()
 
if __name__ == "__main__":
    main();
