import os, re


def find_new_calls(dirn, path, functions_in_use_list, modules_list, pattern, methods_restrictions, passed_modules_links): 
    
    try:
        for i in dirn:
            if os.path.isfile(path+'/'+i): # Нужно проверить - есть файл или нет
            # Только .bsl файлы   
                if i.find('.bsl')!=-1 and i.find('~$')==-1 and i.find('.bsl.')==-1:
                    search_in_file(path+'/'+i, functions_in_use_list, modules_list, pattern, methods_restrictions, passed_modules_links) 
            else:
                # Для случая, когда это директория - вызываем рекурсивный поиск:
                find_new_calls(os.listdir(path+'/'+i),path+'/'+i, functions_in_use_list, modules_list, pattern, methods_restrictions, passed_modules_links)
                                        
    except Exception as err:
        print(err,path+'/'+i)

   
        
def search_in_file(path_file, functions_in_use_list, modules_list, pattern, methods_restrictions, passed_modules_links):

    f = open(path_file, 'r', encoding='utf-8')
    filedata_raw = f.read()
    f.close()

    if len(methods_restrictions) > 0:
        filedata = filter_module_text(filedata_raw, methods_restrictions)
    else:
        filedata = filedata_raw

    module_name = path_file.split('/')[7]
        
    # 1. Выполняем поиск вызовов собственных функций модулей.
    not_first_Step = len(methods_restrictions) > 0

    # Данный список содержит всех методов, используемых в модулях.
    module_methods_list = list()

    template_procedure = 'Процедура .*\('
    template_function = 'Функция .*\('

    # Определяем шаблон регулярного выражения, имеем ли вызов внешнего модуля или своей собственной ф-и.
    regexp_part_call = '\.[ ]*'

    if not_first_Step == True:

        for filedata_line in filedata_raw.splitlines(): # Производим поиск в "неотфильтрованном" файле.

            filedata_line_filtered = re.sub(r'\(.*', r'(', filedata_line)

            # Собираем имена всех методов.
            if re.match(template_procedure, filedata_line_filtered) != None or re.match(template_function, filedata_line_filtered) != None:
                
                method_name = filedata_line_filtered.replace('Процедура ', '').replace('Функция ', '').replace('(', '').strip()
                module_methods_list.append(method_name)
                
                # Поиск вызовов в отфильтрованном (урезанном) модуле.
                allsearcresults_ownfunction = re.findall('.*' + method_name + '.*\(', filedata) 

                for i_raw in allsearcresults_ownfunction:

                    # Если это не определение процедуры/функции и не содержит знака '.':
                    if re.match(template_procedure, i_raw) == None and re.match(template_function, i_raw) == None and re.search(regexp_part_call + method_name, i_raw) == None:
                        
                        own_function_name = module_name + '.' + method_name

                        if not own_function_name in functions_in_use_list:
                            functions_in_use_list.append(own_function_name)
    
                            break # Нет смысла проверять дальше.


    pattern_module = ''
    for module_method_in_list in module_methods_list:
        if pattern_module == '':
            or_char = ''
        else:
            or_char = '|'
            
        pattern_module = pattern_module + or_char + '[ ,]*' + module_method_in_list + '[ ]*\('

    # Соединяем 2 шаблона: для вызовов внутренных ф-й и для вызовов внешних ф-й.
    if pattern == '':
        or_char = ''
    else:
        or_char = '|'
            
    pattern_final = pattern + or_char + pattern_module
    module_isown_string = ''
    
    try:


        allsearcresultsadded = list()
        allsearcresults_modulesadded = list()
            
        allsearcresults = re.findall(pattern_final, filedata)
        allsearcresults_string = ''
        allsearcresults_modules_string = ''

        for i_raw in allsearcresults:

            
            if re.match(template_procedure, i_raw) != None and re.match(template_function, i_raw) != None:
                # Пропускаем определения ф-й или процедур - они не являются вызовами.
                continue
            
            i = prepare_function_call_string(i_raw)
            module_name = path_file.split('/')[7]
            
            
            if len(i.split(".")) == 2:
                # Это вызов внешнего метода. Он имеет формат '<module>.<method>'
                module_isown_string = ''
            else:
                # Это вызов внутреннего метода модуля.
                i = module_name + '.' + i
                module_isown_string = ' (own)'

                    
            if i != '' and not i in allsearcresultsadded:
                allsearcresults_string = allsearcresults_string + '\n' + i
                allsearcresultsadded.append(i)

                if not i in functions_in_use_list: 
                    functions_in_use_list.append(i)
      
            modulestring_toadd = i.split(".")[0]               
            passed_modules_link_add = module_name + '|' + i
                    
            if i != '' and (not passed_modules_link_add in passed_modules_links):
                passed_modules_links.append(passed_modules_link_add)
                print ('        Added a new module: ' + modulestring_toadd + module_isown_string)
                print ('            ' + passed_modules_link_add)
                
                if i != '' and not (modulestring_toadd in allsearcresults_modulesadded):                  
                    allsearcresults_modules_string = allsearcresults_modules_string + '\n' + modulestring_toadd
                    allsearcresults_modulesadded.append(modulestring_toadd)
                    if not modulestring_toadd in modules_list:
                        modules_list.append(modulestring_toadd)


    except Exception as err:
        print(err)



# Метод удаляет некоторые незначящие символы из строки с вызовом метода.
def prepare_function_call_string(i_raw):

    i_raw_lines = i_raw.splitlines()
    if len(i_raw_lines) > 0:
       i = re.sub('\(.*', '(', i_raw_lines[0])
    else:
        i = re.sub('\(.*', '(', i_raw)

    # Удаление некоторых спец. символов
    i = re.sub('\".*', '', i)
    i = re.sub(' *', '', i)
    i = re.sub(' *', '', i)

    return i
    

# Метод фильтрует текст модуля с учетом заданного шаблона , содержащего имена используемых функций.
def filter_module_text(filedata_raw, methods_restrictions):

    module_filtered_text = ''
    used_function_mode = False
    
    for filedata_line in filedata_raw.splitlines():

        if used_function_mode == True:

            module_filtered_text = module_filtered_text + '\n' + filedata_line
            if filedata_line[:12] == 'КонецФункции' or filedata_line[:14] == 'КонецПроцедуры':
                used_function_mode = False
        else:

            for method_restriction in methods_restrictions:
                template_restriction_procedure = method_restriction.replace('(', '')
                
                try:
                    filedata_line_filtered = re.sub(r'\(.*', r'(', filedata_line)
                    
                    if re.match(template_restriction_procedure, filedata_line_filtered) != None:
                        # Функция найдена
                        
                        used_function_mode = True
                        module_filtered_text = module_filtered_text + '\n' + filedata_line_filtered
  
                except Exception as err:
                    print(err)
                    print(template_restriction_procedure + '||' + re.sub('\(.*', '(', filedata_line_filtered))

    return module_filtered_text




def main(dir_path, all_modules_path, path_file_functions_result, path_file_modules_result, path_commonmodules, max_iterations):

    # Определение списка использованных методов в формате: <module>.<function> (CommonUse.ObjectAttributeValue)
    functions_in_use_list = list()

    # Определние списка используемых модулей.
    modules_list = list()

    methods_restrictions = list() # Нет ограничений при первом запуске итераций.
    passed_modules_links = list()
 
    pattern = ''

    # Читаем имена всех общих модулей.
    all_modules_file = open(all_modules_path, 'r', encoding='utf-8');
    all_modules_line = all_modules_file.readline()

    while all_modules_line:
        if pattern == '':
            or_char = ''
        else:
            or_char = '|'
            
        pattern = pattern + or_char + all_modules_line.strip() + '\.[^ (]*\('
        all_modules_line = all_modules_file.readline()

   
    all_modules_file.close()   

    # Выполняем нулевую итерацию.
    print('Step #0 (preparations)')

    # Выполняем поиск вызовов методов в самих переносимых объектах.
    find_new_calls(os.listdir(dir_path), dir_path, functions_in_use_list, modules_list, pattern, methods_restrictions, passed_modules_links);
    result = ''
    for listitem in functions_in_use_list:
        result = result + '\n' + listitem

    # Зададим и будем рассчитывать кол-во циклов, в результате которых не было найдено ни одного нового вызова.
    iterations_withoutadding_methods = 0
    methods_count_previousstep = len(passed_modules_links)

    step_index = 0
    doIterations = True

    # Итоговый список добавленных модулей.
    related_modules_added_total = dict()

    # Выполняем итерации - поиск вызовов в модулях, вызванных переносимым объектом и так далее.
    while doIterations== True:
        
        step_index = step_index + 1
        print('Step #' + str(step_index))

        # Останавливаем выполненеи в случае, если явно задано ограничение.
        if max_iterations != -1 and step_index > max_iterations:
            break
        
        related_modules_added = dict()

        # Поиск в каждом модуле.
        for foundmodule in modules_list:

            # Ищем файлы.
            print('    Working with the module: ' + foundmodule)
            methods_restrictions = list()

            # Проверяем по каждому методу.
            for foundfunction in functions_in_use_list:

                
                if foundfunction.split('.')[0] == foundmodule:
                    # Вызов без "точки" - понимаем, что это вызов собственного метода модуля.
                    
                    method_restriction_toadd = re.sub('\(.*', '', foundfunction.split('.')[1])
                    if method_restriction_toadd != '' and method_restriction_toadd != None:
                        # Это может быть как ф-я , так и процедура
                        new_procedure_restriction = 'Процедура.*' + method_restriction_toadd
                        new_function_restriction = 'Функция.*' + method_restriction_toadd

                        if not new_function_restriction in methods_restrictions:
                            methods_restrictions.append(new_function_restriction)

                        if not new_procedure_restriction in methods_restrictions:
                            methods_restrictions.append(new_procedure_restriction)
        
            dir_path_module = path_commonmodules + '/' + foundmodule

            if os.path.exists(dir_path_module):
                
                methods_count_before = len(passed_modules_links)

                # Ищем только новые вызовы.
                find_new_calls(os.listdir(dir_path_module), dir_path_module, functions_in_use_list, modules_list, pattern, methods_restrictions, passed_modules_links);
                methods_count = len(passed_modules_links)
        
                if methods_count == methods_count_previousstep:
                    iterations_withoutadding_methods = iterations_withoutadding_methods + 1
                else:
                    methods_count_previousstep = methods_count
                    iterations_withoutadding_methods = 0

                    modules_added = methods_count - methods_count_before
                    if foundmodule in related_modules_added:
                        related_modules_added[foundmodule] = related_modules_added[foundmodule] + modules_added
                    else:
                        related_modules_added[foundmodule] = modules_added

                    if foundmodule in related_modules_added_total:
                        related_modules_added_total[foundmodule] = related_modules_added_total[foundmodule] + modules_added
                    else:
                        related_modules_added_total[foundmodule] = modules_added
                        
                print ('        (iterations without adding methods = ' + str(iterations_withoutadding_methods) + ' methods: ' + str(methods_count) + ')')

                if iterations_withoutadding_methods >= methods_count:
                    doIterations = False

        related_modules_added_sorted = {k: related_modules_added[k] for k in sorted(related_modules_added, key=related_modules_added.get, reverse=True)}
        for related_module_added in related_modules_added_sorted.items():
            print ('    Modules added: ' + str(related_module_added))
            
    related_modules_added_total_sorted = {k: related_modules_added_total[k] for k in sorted(related_modules_added_total, key=related_modules_added_total.get, reverse=True)}
    for related_module_added in related_modules_added_total_sorted.items():
        print ('Modules added (TOTAL): ' + str(related_module_added))


        
    # Методы (итого)
    f = open(path_file_functions_result, 'w', encoding='utf-8')

    passed_modules_links_string = ''
    for passed_modules_link in passed_modules_links:
        passed_modules_links_string = passed_modules_links_string + '\n' + passed_modules_link
        
    f.write(passed_modules_links_string)
    f.close()

    # Модули (итого)
    f = open(path_file_modules_result, 'w', encoding='utf-8')
    modules_list_string = ''
    for modules in modules_list:
        modules_list_string = modules_list_string + '\n' + modules

    f.write(modules_list_string)
    f.close()


if __name__ == "__main__":
    
	# Путь к переносимому объекту.
    dir_path = 'C:/Users/user/workspace/TestPython/src/Reports/ОтчетПеренос'
	
	# Путь к общим модулям в проекте.
    path_commonmodules = 'C:/Users/user/workspace/TestPython/src/CommonModules'

	# Путь к списку общих модулей, которые необходимо проверять. Список может быть ограничен вручную.
    all_modules_path = 'C:/Users/user/Documents/Shuvalov-transfer/Release 2.2.9/Перевод отчета/CommonModulesTest.txt'
	
	# Путь к файлу результата - списку вызываемых методов.
    path_file_functions_result = 'C:/Users/user/Documents/Shuvalov-transfer/Release 2.2.9/Перевод отчета/result_functions_test.txt'
	
	# Путь к файлу результата - списку вызываемых модулей.
    path_file_modules_result = 'C:/Users/user/Documents/Shuvalov-transfer/Release 2.2.9/Перевод отчета/result_modules_test.txt'

	# Макс. количество итераций (-1 = нет ограничений)
    max_iterations = -1;

    main(dir_path, all_modules_path, path_file_functions_result, path_file_modules_result, path_commonmodules, max_iterations)
