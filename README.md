Инструменты для отделения выбранных объектов и общих модулей от исходной конфигурации для встраивания в другую конфигурацию.  
Представлен тестовый проект в формате EDT, на котором можно протестировать инструменты.  

Подробное описание см. здесь.
https://gitlab.com/1c_vietnam/develop_software/wiki/-/wikis/Transferring-objects-between-different-configurations-(scripts)