import re, os, shutil
 
def main():

    files_dir = 'C:/Users/user/workspace/TestPython/src/CommonModules'
    all_methods_path = 'C:/Users/user/Documents/Shuvalov-transfer/Release 2.2.9/Перевод отчета/result_functions_test.txt'
    
    methods_list = list()

    all_methods_file = open(all_methods_path, 'r', encoding='utf-8');
    all_methods_line = all_methods_file.readline()

    while all_methods_line:
        all_methods_line_modified = all_methods_line.strip()
        all_methods_line_splitted = all_methods_line_modified.split('|')

        if len(all_methods_line_splitted) == 2:
            methods_list.append(all_methods_line_splitted[1])
                
        all_methods_line = all_methods_file.readline()

    all_methods_file.close()   

    
    # Поиск файлов
    find_file(os.listdir(files_dir), files_dir, methods_list);




def find_file(dirn, path, methods_list): # функция принимает os.listdir, и путь на папку
    
    try:
        for i in dirn:
            if os.path.isfile(path+'/'+i): #проверяем что перед нами, файл или нет
            #Проверяем расширение файла и исключаем системные которые начинаются на [~$]   
                if i.find('.bsl')!=-1 and i.find('~$')==-1 and i.find('.bsl.')==-1 and i.find('_old')==-1:
                    search_in_file(path+'/'+i, methods_list) #Функция по обработке архивов
            else:
                #Если это директория, то проваливаемся в нее
                find_file(os.listdir(path+'/'+i),path+'/'+i, methods_list)
    except Exception as err:
        print(err,path+'/'+i)

   
        
def search_in_file(path_file, methods_list):

    text_result = ''
    
    f = open(path_file, 'r', encoding='utf-8')
    filedata_raw = f.read()
    f.close()
    
    oldmodule_path = os.path.split(path_file)[0] + '/Module_old.bsl'
    if not os.path.exists(oldmodule_path):
        shutil.copyfile(path_file, oldmodule_path)
                    

    path_file_result = os.path.split(path_file)[0] + '/result.txt' 
    
    path_file_splitted = path_file.split('/')

    Template_EndProcedure = 'КонецПроцедуры *'
    Template_EndFunction = 'КонецФункции *'
    
    unused_method = True
    
    modulename_index_in_filename = len(path_file_splitted) - 2
    print(path_file_splitted[modulename_index_in_filename])
    
    for filedata_line in filedata_raw.splitlines():

        for method_tobe_deleted in methods_list:

            method_tobe_deleted_splitted = method_tobe_deleted.split('.')

            if len(method_tobe_deleted_splitted) == 2 and path_file_splitted[modulename_index_in_filename] == method_tobe_deleted_splitted[0]:

                if method_tobe_deleted_splitted[1] == '':
                    continue
                
                template_procedure = 'Процедура *'  + method_tobe_deleted_splitted[1].replace('(', '')
                template_function = 'Функция *'  + method_tobe_deleted_splitted[1].replace('(', '')
                                         
                if re.match(template_procedure, filedata_line) != None or re.match(template_function, filedata_line) != None:
                    unused_method = False
                
        if not unused_method and (re.match(Template_EndProcedure, filedata_line) != None or re.match(Template_EndFunction, filedata_line) != None): 
            text_result = text_result + '\n' + filedata_line
            unused_method = True
            
        if not unused_method:
            text_result = text_result + '\n' + filedata_line


    f = open(path_file, 'w', encoding='utf-8')
    f.write(text_result)
    f.close()



if __name__ == "__main__":
    main();
